# Assignment 3 Part 2

An nginx proxy server that serves our own index.html file and uses it as a reverse proxy for the backend binary service with routes `/hey` and `/echo`.

## Included material

- backend binary, hello-server
- frontend, index.html
- nginx configuration file, hello.conf
- service file for backend, hello-server.service
- config for setting up servers, cloud-config.yml
- example curl commands for testing your server, curl.md

## Directories

- hello-server goes in the directory `/var/www/backend/hello-server`
- index.html goes in the directory `/var/www/frontend/index.html`
- hello.conf goes in the directory `/etc/nginx/sites-available/hello.conf`
- hello-server.service goes in the directory `/etc/systemd/system/hello-server.service`
- create a new folder `hello-server` in the `/var/log` directory for the backend.log file to be created in `/var/log/hello-server/backend.log`
